# _The Phantom_ comic viewer

Hello! I like to read the Phantom. I don't like the Comics Kingdom website. So I made this.

## Building

Note that this was originally built with the public files in `public` instead of `public-vue` and the generated files were in `docs` instead of `public`. I moved them manually when migrating from GitHub to GitLab, and they'll have to be moved back to build a new version.
